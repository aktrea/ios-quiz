//
//  ChallengeViewController.swift
//  Quiz
//
//  Created by Sabari on 16/6/17.
//  Copyright © 2017 aktrea. All rights reserved.
//

import UIKit

class ChallengeViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var getChallengeData = NSArray()
    var challengeName = NSArray()
    @IBOutlet var challengeTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.challengeTable.backgroundColor = UIColor.clear
        self.getData()
        self.navigationItem.title = "Challenges"

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.challengeName.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.challengeTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let challengeName = cell.contentView.viewWithTag(1) as! UILabel
        challengeName.text = self.challengeName[indexPath.row] as? String
        cell.backgroundColor = UIColor.darkGray
        challengeName.textColor = UIColor.white
        challengeName.textAlignment = .center
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let value = self.getChallengeData[indexPath.row] as! NSDictionary
        self.performSegue(withIdentifier: "quizQuestion", sender: value )
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "quizQuestion" {
            let quizView = segue.destination as! QuizController
            quizView.getQuizData = sender as! NSDictionary
        }
    }
    func getData()
    {
        let urlValue = URL(string: "http://192.168.3.139/akton/api/challenge/getallchallenges?moduleId=1020")!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler: {
            (data,response,error) in
            guard error == nil else
            {
                print(error?.localizedDescription as Any)
                return
            }
            do
            {
                self.getChallengeData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSArray
                print(self.getChallengeData)
                self.challengeName = self.getChallengeData.value(forKey: "ChallengeName") as! NSArray
                
                DispatchQueue.main.async {
                    self.challengeTable.reloadData()
                }
            }
            catch
            {
                print(error.localizedDescription)
            }
        })
        task.resume()
        
    }

}
