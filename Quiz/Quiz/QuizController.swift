//
//  QuizController.swift
//  Quiz
//
//  Created by Sabari on 20/6/17.
//  Copyright © 2017 aktrea. All rights reserved.
//

import UIKit
import CoreData

class QuizController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var blur: UIView!
    @IBOutlet var quizTable: UITableView!
    var getQuizData = NSDictionary()
    @IBOutlet weak var questionHeight: NSLayoutConstraint!
    @IBOutlet weak var questionText: UILabel!
    var index = Int()
    var quizQuestion = NSArray()
    var Questions =  NSDictionary()
    var Choice = NSArray()
    var questionChoice = [[[String:AnyObject]]]()
    //colors
    var questionColor = UIColor()
    var answerColor = UIColor()
    var nextButColor = UIColor()
    var preButColor = UIColor()
    var submitButColor = UIColor()
    var tableBgColor = UIColor()
    var tableCellColor = UIColor()
    var butTitleColor = UIColor()
    var backGroundImage = UIImageView()
    
    @IBOutlet var submitBut: UIButton!
    @IBOutlet var previousBut: UIButton!
    @IBOutlet var nextBut: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.allColor()
        self.navigationItem.title = self.getQuizData.value(forKey: "ChallengeName") as? String
        self.quizQuestion = self.getQuizData.value(forKey: "ChallengeQuestions") as! NSArray
        
        self.questionChoice = self.quizQuestion.value(forKey: "QuestionChoices") as! [[[String:AnyObject]]]
        index = 0
        
        
        previousBut.layer.cornerRadius = 5
        previousBut.layer.masksToBounds = true
        nextBut.layer.cornerRadius = 5
        nextBut.layer.masksToBounds = true
        submitBut.layer.cornerRadius = 5
        submitBut.layer.masksToBounds = true
        self.question()
        
        print(self.getQuizData)
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blur.addSubview(blurEffectView)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setColor()
    {
        self.quizTable.backgroundColor = tableBgColor
        self.nextBut.backgroundColor = nextButColor
        self.previousBut.backgroundColor = preButColor
        self.submitBut.backgroundColor = submitButColor
        self.backImage.image = UIImage(named:"1.jpg")
    }
    func allColor()
    {
        questionColor = UIColor(colorLiteralRed: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        answerColor = UIColor(colorLiteralRed: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        tableBgColor = UIColor.clear
        preButColor = UIColor(colorLiteralRed: 85/255, green: 85/255, blue: 85/255, alpha: 1)
        nextButColor = UIColor(colorLiteralRed: 85/255, green: 85/255, blue: 85/255, alpha: 1)
        submitButColor = UIColor(colorLiteralRed: 0/255, green: 128/255, blue: 0/255, alpha: 1)
        butTitleColor = UIColor(colorLiteralRed: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        
        
        
       
        setColor()
    }
func question()
{
    let question = self.quizQuestion[index] as! NSDictionary
    let height:CGFloat = self.calculateHeight(inString: (question.value(forKey: "Question") as? String)!)
    questionHeight.constant = height+20
    questionText.text = question.value(forKey: "Question") as? String
    questionText.textColor = questionColor

    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        let question = self.quizQuestion[index] as! NSDictionary
        self.Choice = question.value(forKey: "QuestionChoices") as! NSArray
        return Choice.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = quizTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Answer1TableViewCell
        
        let question = self.quizQuestion[index] as! NSDictionary
        let choices = question.value(forKey: "QuestionChoices") as! NSArray
        let choice = choices[indexPath.row] as! NSDictionary
        cell.answerOptionTxt.text = choice.value(forKey: "Choice") as? String
        cell.answerOptionTxt.textColor = answerColor
        if (self.quizQuestion[index] as AnyObject).value(forKey: "Type") as! Int == 0
        {
            self.quizTable.allowsMultipleSelection = false
            cell.buttonView.layer.cornerRadius = 11
            cell.buttonView.layer.masksToBounds = true
            if questionChoice[index][indexPath.row]["IsUserSelect"] as! Bool == true
            {
                cell.buttonView.backgroundColor = UIColor.green
                 tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            }
            else
            {
                cell.buttonView.backgroundColor = UIColor(colorLiteralRed: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
            }

        }
        else if (self.quizQuestion[index] as AnyObject).value(forKey: "Type") as! Int == 1
        {
            self.quizTable.allowsMultipleSelection = true
            cell.buttonView.layer.cornerRadius = 5
            cell.buttonView.layer.masksToBounds = true
            if questionChoice[index][indexPath.row]["IsUserSelect"] as! Bool == true
            {
                cell.buttonView.backgroundColor = UIColor.green
               
             //  tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
              // self.quizTable(self.quizTable s)
            }
            else
            {
                cell.buttonView.backgroundColor = UIColor(colorLiteralRed: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
            }

        }
        return cell
        
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let question = self.quizQuestion[index] as! NSDictionary
        let choices = question.value(forKey: "QuestionChoices") as! NSArray
        let choice = choices[indexPath.row] as! NSDictionary
        let answers = choice.value(forKey: "Choice") as! String
        let height:CGFloat = self.calculateHeight(inString: answers)
        return height + 40.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(" Selected\(indexPath.row)")
        questionChoice[index][indexPath.row]["IsUserSelect"] = true as Bool as AnyObject
        if let cell : Answer1TableViewCell = tableView.cellForRow(at: indexPath) as? Answer1TableViewCell {
            if (self.quizQuestion[index] as AnyObject).value(forKey: "Type") as! Int == 0
            {
               
                cell.buttonView.backgroundColor = UIColor.green
                
            }
            else if (self.quizQuestion[index] as AnyObject).value(forKey: "Type") as! Int == 1
            {
                
                    cell.buttonView.backgroundColor = UIColor.green
                
            }

            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.06
            animation.repeatCount = 2
            animation.autoreverses = true
            animation.fromValue = CGPoint(x: cell.backView.center.x - 10, y: cell.backView.center.y)
            animation.toValue = CGPoint(x: cell.backView.center.x + 10, y:cell.backView.center.y)
            cell.backView.layer.add(animation, forKey: "position")
            
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print(" DeSelected\(indexPath.row)")
        if let cell : Answer1TableViewCell = tableView.cellForRow(at: indexPath) as? Answer1TableViewCell {

        if (self.quizQuestion[index] as AnyObject).value(forKey: "Type") as! Int == 0
        {
            cell.buttonView.backgroundColor = UIColor(colorLiteralRed: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
        }
        else if (self.quizQuestion[index] as AnyObject).value(forKey: "Type") as! Int == 1
        {
            cell.buttonView.backgroundColor = UIColor(colorLiteralRed: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
        }

         questionChoice[index][indexPath.row]["IsUserSelect"] = false as Bool as AnyObject
           
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        // This will create a "invisible" footer
        return 0.01
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func calculateHeight(inString:String) -> CGFloat
    {
        let messageString = inString
        let attributes : [String : Any] = [NSFontAttributeName : UIFont.systemFont(ofSize: 15.0)]
        
        let attributedString : NSAttributedString = NSAttributedString(string: messageString, attributes: attributes)
        
        let rect : CGRect = attributedString.boundingRect(with: CGSize(width: 222.0, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        
        let requredSize:CGRect = rect
        return requredSize.height
    }
    @IBAction func previousButton(_ sender: Any) {
        if index == 0
        {
            
        }
        else if index <= self.quizQuestion.count
        {
            index -= 1
            self.question()
            self.quizTable.reloadData()
            let animation = CATransition()
            animation.type = kCATransitionPush
            animation.subtype = kCATransitionFromLeft
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            animation.fillMode = kCAFillModeBoth
            animation.duration = 0.8
            self.view.layer.add(animation, forKey: "UITableViewReloadDataAnimationKey")
                    }
        
    }
    @IBAction func nxtButton(_ sender: Any) {
        
        if index == self.quizQuestion.count-1
        {
            
        }
        else if index >= 0
        {
            index += 1
            self.question()
            self.quizTable.reloadData()
            let animation = CATransition()
            animation.type = kCATransitionPush
            animation.subtype = kCATransitionFromRight
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            animation.fillMode = kCAFillModeBoth
            animation.duration = 0.8
            self.view.layer.add(animation, forKey: "UITableViewReloadDataAnimationKey")
        }
        
    }

    @IBAction func subButton(_ sender: Any) {
        
        var finalArray = [[String:AnyObject]]()
        for i in 0..<questionChoice.count
        {
            for j in 0..<questionChoice[i].count
            {
               finalArray.append(questionChoice[i][j])
            }
        }
        print(finalArray)
        let finalJson = ["User Answer" : finalArray] as NSDictionary
        submitData(answerData: finalJson)
    }
    func submitData(answerData : NSDictionary)
    {
    //send answer json data to server
    let answerJsonData = try? JSONSerialization.data(withJSONObject: answerData, options: .prettyPrinted)
    let decodedValue = try? JSONSerialization.jsonObject(with: answerJsonData!, options: []) as! NSDictionary
    
    let answerJsonData1 = try? JSONSerialization.data(withJSONObject: decodedValue! , options: .prettyPrinted)
    let urlValue = URL(string:"http://192.168.3.139/akton/api/SubmitChallenge")!
    var request = URLRequest(url: urlValue)
    request.httpMethod="POST"
    request.httpBody=answerJsonData1
        
        
    //set content type
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    let session = URLSession.shared
    let task = session.dataTask(with: request, completionHandler:
    {
        (data, response, error)in
        if(error != nil)
        {
            print(error!)
        }
        else
        {
            do
            {
                //get response from server as String
                let PostResponse = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as NSString
                print(PostResponse)
            }
        }
        })
        task.resume()
    }
func loginData()
{
    let urlValue = URL(string: "")!
    var request = URLRequest(url: urlValue)
    request.httpMethod = "POST"
    let paramString = ["1":"234"] 
    request.httpBody = try? JSONSerialization.data(withJSONObject: paramString, options: .prettyPrinted)
    request.addValue("", forHTTPHeaderField: "")
    let session = URLSession.shared
    let task = session.dataTask(with: request, completionHandler: {
        (data,response,error)in
        
        let dataResponse = String(data: data!, encoding: .utf8)
        let alert = UIAlertController(title: "Success", message: "You have successfully Completed your challenge", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default){ (result : UIAlertAction) -> Void in
        }
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    })
    
    task.resume()
    }
    
}


