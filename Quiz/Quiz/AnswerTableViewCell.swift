//
//  AnswerTableViewCell.swift
//  Quiz
//
//  Created by Sabari on 19/6/17.
//  Copyright © 2017 aktrea. All rights reserved.
//

import UIKit

class AnswerTableViewCell: UITableViewCell {

   
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tickImage: UIImageView!
    @IBOutlet var cornerView: UIView!
    @IBOutlet var answerOptionTxt: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.borderColor = UIColor.lightGray.cgColor
        mainView.layer.borderWidth = 1
        cornerView.layer.cornerRadius = 0
        cornerView.layer.masksToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected
        {
            tickImage.isHidden = false
        }
        else
        {
            tickImage.isHidden = true
            
        }
               // Configure the view for the selected state
    }

}
