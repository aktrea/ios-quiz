//
//  LoginViewController.swift
//  SuperSaver2
//
//  Created by Sabari on 15/2/17.
//  Copyright © 2017 aktrea. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet var userName: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var loginButton: UIButton!

    @IBAction func loginButton(_ sender: Any) {
        self.view.endEditing(true)
        self.view.frame.origin.y = 0
        loginData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.delegate=self
        password.delegate=self
        
        
    }
    // Do any additional setup after loading the view, typically from a nib.
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*!
     *@brief While enter username and password keyboardDidShow fuction execute(NSNotification).
     */
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.autocorrectionType = .no
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardDidShow), name:NSNotification.Name.UIKeyboardDidShow, object: nil);
        return true
        
    }
    /*!
     *@brief keyboard disappear when click return/done button
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    /*!
     *@brief after enter username and password keyboardDidHide fuction execute(NSNotification).
     */
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.autocorrectionType = .no
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardDidHide), name:NSNotification.Name.UIKeyboardDidHide, object: nil);
        return true
        
        
    }
    /*!
     *@brief hide statusbar
     */
    override var prefersStatusBarHidden: Bool {
        return true
    }
    /*!
     *@brief move view to top when keyboard appears
     */
    func keyboardDidShow(notification:NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if(self.view.frame.origin.y == 0)
            {
                if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad)
                {
                self.view.frame.origin.y -= 150
                }
                else
                {
                    self.view.frame.origin.y -= 70
                }
            }
        }
        
    }
    /*!
     *@brief set view to orginal height when keyboard hide
     */
    func keyboardDidHide(notification:NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if(self.view.frame.origin.y != 0)
            {
                if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad)
                {
                    self.view.frame.origin.y += 150
                }
                else
                {
                    self.view.frame.origin.y += 70
                }
            }
        }
        
    }
    /*!
     *@brief login using URLSession pass username and password get response from server save login userid in UserDefaults
     */
    func loginData()
    {
        
        let urlValue=URL(string: "http://192.168.3.128/minerva/api/LoginApi/")!
        var request=URLRequest(url: urlValue)
        let session=URLSession.shared
        request.httpMethod="POST"
        let paramString="Email=\(userName.text!)&Password=\(password.text!)"
        print(paramString)
        request.httpBody=paramString.data(using: String.Encoding.utf8)
        let task = session.dataTask(with: request)
        {
            (data, response, error) in
            guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil
                else
            {
                print("error")
                return
            }
            //remove '"' from string
            var dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            dataString = dataString?.replacingOccurrences(of: "\"", with: "") as NSString?
            print(dataString!)
            if(dataString == "failure" || dataString == "")
            {
                
                
                Util.showNotification(title: "Sorry",subTitle: "Please Check username and password",color: UIColor.red)
            }
            else
                {
            //seperate string to get userid
            let userValue=dataString?.components(separatedBy: "_")
            //save userid in UserDefaults
            let userId = UserDefaults.standard
            userId.set(userValue![1], forKey: "userid")
            userId.synchronize()
           
            DispatchQueue.main.async(){
                self.performSegue(withIdentifier: "loginSuccess", sender:nil)
            }
            }
            
        }
        task.resume()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "loginSuccess")
        {
            DispatchQueue.main.async(){
                _ = segue.destination 
            }
        }
    }
   }
