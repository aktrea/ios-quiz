//
//  Answer1TableViewCell.swift
//  Quiz
//
//  Created by Sabari on 20/6/17.
//  Copyright © 2017 aktrea. All rights reserved.
//

import UIKit

class Answer1TableViewCell: UITableViewCell {

    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var answerOptionTxt: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
