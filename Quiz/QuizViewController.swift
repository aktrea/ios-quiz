//
//  QuizViewController.swift
//  Quiz
//
//  Created by Sabari on 13/6/17.
//  Copyright © 2017 aktrea. All rights reserved.
//

import UIKit

class QuizViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate{
    var getQuizData = NSDictionary()
    @IBOutlet var quizTable: UITableView!
    var index = Int()
    var quizQuestion = NSArray()
    var Questions =  NSDictionary()
    var Choice = NSArray()
    var questionColor = UIColor()
    
    @IBOutlet var submitBut: UIButton!
    @IBOutlet var previousBut: UIButton!
    @IBOutlet var nextBut: UIButton!
    @IBOutlet weak var questionCountView: UIView!
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        questionColor = UIColor.blue
//        navigationController?.navigationBar.barTintColor = UIColor.darkGray
        self.navigationItem.title = self.getQuizData.value(forKey: "ChallengeName") as? String
        self.quizQuestion = self.getQuizData.value(forKey: "ChallengeQuestions") as! NSArray
        
        print(getQuizData)
        index = 0
        
        questionCountView.layer.cornerRadius = 40
        questionCountView.layer.masksToBounds = true
        previousBut.layer.cornerRadius = 5
        previousBut.layer.masksToBounds = true
        nextBut.layer.cornerRadius = 5
        nextBut.layer.masksToBounds = true
        submitBut.layer.cornerRadius = 5
        submitBut.layer.masksToBounds = true

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let question = self.quizQuestion[index] as! NSDictionary
       self.Choice = question.value(forKey: "QuestionChoices") as! NSArray
        return Choice.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = quizTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AnswerTableViewCell
     
        let question = self.quizQuestion[index] as! NSDictionary
        let choices = question.value(forKey: "QuestionChoices") as! NSArray
        let choice = choices[indexPath.row] as! NSDictionary
        cell.answerOptionTxt.text = choice.value(forKey: "Choice") as? String
        
        return cell
        
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let question = self.quizQuestion[index] as! NSDictionary
        let height:CGFloat = self.calculateHeight(inString: (question.value(forKey: "Question") as? String)!)
        let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: height + 40.0))
        let questionLabel = UILabel.init(frame: CGRect(x: 10, y: 0, width: headerView.frame.width, height: headerView.frame.height))
        headerView.addSubview(questionLabel)
        questionLabel.numberOfLines = 0
        questionLabel.text = question.value(forKey: "Question") as? String
        headerView.backgroundColor = UIColor.darkGray
        questionLabel.textColor = UIColor.white
        return headerView
    }
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return questionArray[section]
//    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let question = self.quizQuestion[index] as! NSDictionary
        let value = question.value(forKey: "Question") as! String
        let height:CGFloat = self.calculateHeight(inString: value)
        return height + 40.0
}
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let question = self.quizQuestion[index] as! NSDictionary
        let choices = question.value(forKey: "QuestionChoices") as! NSArray
        let choice = choices[indexPath.row] as! NSDictionary
        let answers = choice.value(forKey: "Choice") as! String
        let height:CGFloat = self.calculateHeight(inString: answers)
        return height + 40.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = quizTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AnswerTableViewCell
       
//        if let cell = tableView.cellForRow(at: indexPath) {
//            cell.answerMar
//        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        if let cell = tableView.cellForRow(at: indexPath) {
//            cell.accessoryType = .none
//            
//        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        // This will create a "invisible" footer
        return 0.01
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func calculateHeight(inString:String) -> CGFloat
    {
        let messageString = inString
        let attributes : [String : Any] = [NSFontAttributeName : UIFont.systemFont(ofSize: 15.0)]
        
        let attributedString : NSAttributedString = NSAttributedString(string: messageString, attributes: attributes)
        
        let rect : CGRect = attributedString.boundingRect(with: CGSize(width: 222.0, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        
        let requredSize:CGRect = rect
        return requredSize.height
    }
    @IBAction func previousButton(_ sender: Any) {
        if index == 0
        {
            
        }
        else if index <= self.quizQuestion.count
        {
            index -= 1
            self.quizTable.reloadData()
        }

    }
    @IBAction func nxtButton(_ sender: Any) {
       
        if index == self.quizQuestion.count-1
        {
            
        }
        else if index >= 0
        {
            index += 1
            self.quizTable.reloadData()
        }
        
    }
       /* func submitData()
    {
        
        let urlValue = URL(string: "")!
        var urlRequest = URLRequest(url: urlValue)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = ""
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest, completionHandler: {(data,response,error)in
            guard error == nil else
            {
                print(error?.localizedDescription as Any)
                
            }
            let resultFromServer = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print(resultFromServer!)
        })
        task.resume()
    }*/
}
